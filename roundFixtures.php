<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<?php
require_once("nocache.php");
require_once('dbconn.php');
session_start();
?> 

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
        <title>NRL Fixtures</title>
    </head>
    <header id="nav" class="centre">
        <ul>
            <!-- The following code was inspired from https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar -->
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn">Fixtures</a>
                <div class="dropdown-content">
                    <a href="roundFixtures.php">Rounds Fixtures</a>
                    <a href="teamFixtures.php">Team Fixtures</a>
                </div>
            </li> <!-- End of code -->
            <li><a href="ladder.php">Ladder</a></li>
            <li><a href="scoreEntry.php">Enter Results</a></li>
            <?php 
            if(isset($_SESSION['who'])) { ?>
            <li><a href="logoff.php">Log Off</a></li>
            <?php
            }
            ?>
        </ul>
    </header>
    <body>
        <div class="centre">
            <h1>2017 NRL Fixtures</h1>

            <!-- Option to select round for fixtures -->
            <form id="teamView" action="roundFixtures.php" method="get">
                <label for="roundNum">Round Number:</label>
                <select name="roundNum" id="roundNum">
                    <?php 
                    for ($i = 1; $i <= 26; $i++) {
                        
                        /* The following code was modified from
                    http://stackoverflow.com/questions/7841464/keep-the-selected-value-for-dropdown-after-submit 
                    */
                        echo "<option value=\"".$i."\"";
                        if(!isset($_GET["Change_Round"])) {     // Checking whether user changed round
                            if($_SESSION['currentRound'] == $i) // to echo selected or not
                                echo 'selected';
                        }
                        else {
                            if($_GET["roundNum"] == $i)
                                echo 'selected';
                        }
                        echo ">".$i."</option>";
                    }   // End of code
                    ?>
                </select>
                <input type="submit" value="Change Round" name="Change Round">
            </form>
            <table>
                <tr>
                    <th>Home Team</th>
                    <th>Score</th>
                    <th>Away Team</th>
                    <th>Match Date</th>
                    <th>Match Time</th>
                    <th>Venue</th>
                </tr>
                <?php
                $i = $_SESSION["currentRound"];
                if (isset($_GET["Change_Round"])) {
                    $i = $_GET["roundNum"];
                }
                echo "<h2 class=nonHighLight>ROUND $i</h2>";

                /* The following code was inspired from 
            http://stackoverflow.com/questions/42189707/sql-select-query-to-display-name-from-another-table-in-two-seperate-columns
            */
                $sql = "SELECT f.matchID, t1.teamID, t1.abbreviatedName AS homeTeam, t1.logo AS homeLogo, t2.teamID, t2.abbreviatedName AS awayTeam, t2.logo AS awayLogo, f.roundID, score1, score2, matchDate, matchTime, f.venue, groundName, ground.groundID
                FROM fixtures f
                JOIN team t1 ON f.homeTeam = t1.teamID
                JOIN team t2 ON f.awayTeam = t2.teamID
                INNER JOIN ground ON f.venue = ground.groundID
                WHERE f.roundID = '$i'"; // End of code
                $results = mysqli_query($dbConn, $sql)
                    or die ('Problem with query' . mysqli_error());            
                while ($row = mysqli_fetch_array($results)) { ?>
                <tr>
                    <td><img src="images/<?php echo $row["homeLogo"]?>" style="width:60px;"><?php echo $row["homeTeam"]?></td>

                    <!-- Else statements to check if match has completed or not -->
                    <td><?php 
                    if($row["score1"] == '') {
                            echo "<p class=scoreV>V</p>";
                    } else {
                        echo $row["score1"]?> - <?php echo $row["score2"]; 
                    }
                        ?></td>

                    <td><?php echo $row["awayTeam"]?><img src="images/<?php echo $row["awayLogo"]?>" style="width:60px;"></td>

                    <td><?php 
                            if(!$row["score1"] == '' || $row["matchDate"] < $_SESSION["todayDate"]) {
                                echo "<p class=fullTime>FULL TIME</p>";   // If score exists means the game has finished
                            } else 
                                echo $row["matchDate"] ?></td>

                    <td><?php 
                                if(!$row["score1"] == '' || $row["matchDate"] < $_SESSION["todayDate"]) {
                                    echo "";
                                } else 
                                    echo $row["matchTime"] ?></td>

                    <td><a href='ground.php?groundID=<?php echo $row["groundID"]?>' target="_blank"><?php echo $row["groundName"]?></a></td>
                </tr>
                <?php
                                                            }
                ?>
            </table>
        </div>
    </body>
</html>