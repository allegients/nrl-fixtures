<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<!-- The following code was modified from Lecture 11 notes login.php
https://vuws.westernsydney.edu.au/bbcswebdav/pid-2772093-dt-content-rid-22888201_1/xid-22888201_1 -->
<?php
require_once("nocache.php");

session_start();
$_SESSION['errorMessage'] = '';

// check that form has been submitted
if(isset($_POST['submit'])) {

    // check that username and password were entered
    if(empty($_POST['email']) || empty($_POST['pword'])) {
        $_SESSION['errorMessage'] = "You have failed the authentication due to entering an incorrect E-mail or Password";
            header("location: index.php");
    } else {
        // connect to the database
        require_once('dbConn.php');

        // parse username and password for special characters
        $email = mysqli_real_escape_string($dbConn, $_POST['email']);
        $password = mysqli_real_escape_string($dbConn, $_POST['pword']);

        // hash the password so it is not readable and encrypted
        $hashedPassword = hash('sha256', $password);

        // query the db
        $sql = "SELECT id, email, password
        FROM user 
        WHERE email = '$email' AND password = '$hashedPassword'";
        $rs = mysqli_query($dbConn, $sql);

        // check number of rows in record set. What does this mean in this context?
        if(mysqli_num_rows($rs)>0) {
            // start a new session for the user
            session_start();

            // Store the user details in session variables
            $user = mysqli_fetch_assoc($rs);
            $_SESSION['who'] = $user['email'];
            // Redirect the user to the secure page
            header('Location: scoreEntry.php');

        } else {
            $_SESSION['errorMessage'] = "You have failed the authentication due to entering an incorrect E-mail or Password";
                header("location: index.php");
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="styles.css">
        <title>Login Form</title>
        <style>
            label { display:inline-block; width:150px; }
            .input-box {padding:5px;}
        </style>
    </head>
    <header id="nav" class="centre">
        <nav>
            <ul>
                <!-- The following code was inspired from https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar -->
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropbtn">Fixtures</a>
                    <div class="dropdown-content">
                        <a href="roundFixtures.php">Rounds Fixtures</a>
                        <a href="teamFixtures.php">Team Fixtures</a>
                    </div>
                </li> <!-- End of code -->
                <li><a href="ladder.php">Ladder</a></li>
                <li><a href="scoreEntry.php">Enter Results</a></li>
                <?php 
                if(isset($_SESSION['who'])) { ?>
                <li><a href="logoff.php">Log Off</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>
    </header>
    <body>
        <div class="centre">
            <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                <div class="input-box">
                    <label for="email">Email:</label>
                    <input type="text" name="email" maxlength="50" id="email">
                </div>
                <div class="input-box">
                    <label for="pword">Password:</label>
                    <input type="password" name="pword" maxlength="20" id="pword">
                </div>
                <div class="input-box">
                    <input type="submit" value="Login" name="submit">
                </div>
            </form>
        </div>
    </body>
</html> <!-- End of code -->