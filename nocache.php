<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<!-- The following code was modified from Week 11 Lecture Notes nocache.php from vUWS
https://vuws.westernsydney.edu.au/bbcswebdav/pid-2772093-dt-content-rid-22888201_1/xid-22888201_1 -->
<?php
// tell the browser not to cache the page
header("Cache-Control: no-cache");

// expire the page as soon as it is loaded
header("Expires: -1");
// End of code ?> 