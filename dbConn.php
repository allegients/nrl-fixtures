<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<!-- The following code was modified from Week 11 Lecture Notes dbconn.php from vUWS
https://vuws.westernsydney.edu.au/bbcswebdav/pid-2772093-dt-content-rid-22888201_1/xid-22888201_1 -->
<?php
// create database connection 
$dbConn = mysqli_connect("localhost", "twa044", "twa044jh", "NRL2017044");
if(!$dbConn) {
    exit("Failed to connect to database " . mysqli_error($dbConn));
}
// End of code ?>