<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<!-- The following code was modified from Lecture 11 notes logoff.php
https://vuws.westernsydney.edu.au/bbcswebdav/pid-2772093-dt-content-rid-22888201_1/xid-22888201_1 -->
<?php
   // ensure the page is not cached
   require_once("nocache.php");

   // get access to the session variables
   session_start();

   // Now destroy the session
   session_destroy();

   // Redirect the user to the starting page (login.php)
   header("location: index.php");
?>