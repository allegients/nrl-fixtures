<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<?php
require_once("nocache.php");    // Making sure website has no cache
require_once('dbConn.php');     // Connecting to the database

if (isset($_GET["Change_Team"])) {   // Checking if there is a value to prevent error
    $userTeam = $_GET["teamFixture"];       // Variable for user chosen team
}
else { 
    $userTeam = 0;
}

session_start();        // Starting session
$currentRound = $_SESSION['currentRound'];
?> 

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
        <title>NRL Fixtures</title>
    </head>
    <header id="nav" class="centre">
        <ul>
            <!-- The following code was inspired from https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar -->
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn">Fixtures</a>
                <div class="dropdown-content">
                    <a href="roundFixtures.php">Rounds Fixtures</a>
                    <a href="teamFixtures.php">Team Fixtures</a>
                </div>
            </li> <!-- End of code -->
            <li><a href="ladder.php">Ladder</a></li>
            <li><a href="scoreEntry.php">Enter Results</a></li>
            <?php 
            if(isset($_SESSION['who'])) { ?>
            <li><a href="logoff.php">Log Off</a></li>
            <?php
            }
            ?>
        </ul>
    </header>
    <body>
        <div class="centre">
            <h1>2017 NRL Fixtures</h1>

            <!-- Form to change tables when user selects different teams -->
            <form id="teamView" action="teamFixtures.php" method="get">

                <?php // Query DB for team names
                $sql = "SELECT * FROM team";
                $results = mysqli_query($dbConn, $sql)
                    or die ('Problem with query' . mysqli_error());
                ?>

                <!-- Selector for viewing by team -->
                <label for="teamFixture">Team:</label>
                <select name="teamFixture" id="teamFixture">
                    <option value="allTeams">All Teams</option>
                    <?php
                    while ($row = mysqli_fetch_array($results)) {

                        /* The following code was modified from
                    http://stackoverflow.com/questions/7841464/keep-the-selected-value-for-dropdown-after-submit 
                    */
                        echo "<option value=\"".$row["teamID"]."\"";
                        if($_GET['teamFixture'] == $row['teamID'])
                            echo 'selected';
                        echo ">".$row["teamName"]."</option>";  // End of code 
                    } 
                    ?>
                </select>
                <input type="submit" value="Change Team" name="Change Team">
            </form>

            <?php
            // Loop for all fixtures from all teams and every round
            if (!$userTeam > 0 || $userTeam == "allTeams") {  
                for ($i = 1; $i <= 26; $i++) { ?>
            <table>
                <tr>
                    <th>Home Team</th>
                    <th>Score</th>
                    <th>Away Team</th>
                    <th>Match Date</th>
                    <th>Match Time</th>
                    <th>Venue</th>
                </tr>
                <?php
                    if ($i == $currentRound) {      // Whether or not to highlight current round
                        echo "<h2 class=roundHighLight>ROUND $i</h2>";
                    }
                    else {
                        echo "<h2 class=nonHighLight>ROUND $i</h2>";
                    }

                    /* The following code was inspired from 
            http://stackoverflow.com/questions/42189707/sql-select-query-to-display-name-from-another-table-in-two-seperate-columns
            */
                    $sql = "SELECT f.matchID, t1.teamID, t1.abbreviatedName AS homeTeam, t1.logo AS homeLogo, t2.teamID, t2.abbreviatedName AS awayTeam, t2.logo AS awayLogo, f.roundID, score1, score2, matchDate, matchTime, f.venue, groundName, ground.groundID
                FROM fixtures f
                JOIN team t1 ON f.homeTeam = t1.teamID
                JOIN team t2 ON f.awayTeam = t2.teamID
                INNER JOIN ground ON f.venue = ground.groundID
                WHERE f.roundID = '$i'"; // End of code
                    $results = mysqli_query($dbConn, $sql)
                        or die ('Problem with query' . mysqli_error());            
                    while ($row = mysqli_fetch_array($results)) { ?>
                <tr>
                    <td><img src="images/<?php echo $row["homeLogo"]?>" style="width:50px; length:100px;"><?php echo $row["homeTeam"]?></td>

                    <!-- Else statements to check if match has completed or not -->
                    <td><?php 
                        if($row["score1"] == '') {
                            echo "<p class=scoreV> V</p>";
                        } else {
                            echo $row["score1"]?> - <?php echo $row["score2"]; 
                        }
                        ?></td>

                    <td><?php echo $row["awayTeam"]?><img src="images/<?php echo $row["awayLogo"]?>" style="width:50px; length:100px;"></td>
                    <td><?php 
                            if(!$row["score1"] == '' || $row["matchDate"] < $_SESSION["todayDate"]) {
                                echo "<p class=fullTime>FULL TIME</p>";   // If score exists means the game has finished
                            } else 
                                echo $row["matchDate"] ?></td>
                    <td><?php 
                                if(!$row["score1"] == '' || $row["matchDate"] < $_SESSION["todayDate"]) {
                                    echo "";
                                } else 
                                    echo $row["matchTime"] ?></td>
                    <td class='venue'><a href='ground.php?groundID=<?php echo $row["groundID"]?>' target="_blank"><?php echo $row["groundName"]?></a></td>
                </tr>
                <?php
                                                                }
                }
            }
                ?>
            </table> 

            <?php
            if ($userTeam > 0) {        // Loop for specfic team's fixtures
                for ($i = 1; $i <= 26; $i++) { ?>

            <?php
                    /* The following code was inspired from 
            http://stackoverflow.com/questions/42189707/sql-select-query-to-display-name-from-another-table-in-two-seperate-columns
            */
                    $sql = "SELECT f.matchID, t1.teamID, t1.abbreviatedName AS homeTeam, t1.logo AS homeLogo, t2.teamID, t2.abbreviatedName AS awayTeam, t2.logo AS awayLogo, f.roundID, score1, score2, matchDate, matchTime, f.venue, groundName, ground.groundID
                FROM fixtures f
                JOIN team t1 ON f.homeTeam = t1.teamID
                JOIN team t2 ON f.awayTeam = t2.teamID
                INNER JOIN ground ON f.venue = ground.groundID
                WHERE (t1.teamID = '$userTeam' OR t2.teamID = '$userTeam')
                AND f.roundID = '$i'"; // End of code   
                    $results = mysqli_query($dbConn, $sql)
                        or die ('Problem with query' . mysqli_error());            
                    while ($row = mysqli_fetch_array($results)) { ?>
            <table>
                <tr>
                    <th>Home Team</th>
                    <th>Score</th>
                    <th>Away Team</th>
                    <th>Match Date</th>
                    <th>Match Time</th>
                    <th>Venue</th>
                </tr>
                <?php
                        if ($i == $currentRound) {      // Whether or not to highlight current round
                            echo "<h2 class=roundHighLight>ROUND $i</h2>";
                        }
                        else {
                            echo "<h2 class=nonHighLight>ROUND $i</h2>";
                        }
                ?>
                <tr>
                    <td><img src="images/<?php echo $row["homeLogo"]?>" style="width:60px;"><?php echo $row["homeTeam"]?></td>

                    <!-- Else statements to check if match has completed or not -->
                    <td><?php 
                    if($row["score1"] == '') {
                        echo "<p class=scoreV>V</p>";
                    } else {
                        echo $row["score1"]?> - <?php echo $row["score2"]; 
                    }
                        ?></td>

                    <td><?php echo $row["awayTeam"]?><img src="images/<?php echo $row["awayLogo"]?>" style="width:50px; length:100px"></td>

                    <td><?php 
                            if(!$row["score1"] == '' || $row["matchDate"] < $_SESSION["todayDate"]) {
                                echo "<p class=fullTime>FULL TIME</p>";   // If score exists means the game has finished
                            } else 
                                echo $row["matchDate"] ?></td>

                    <td><?php 
                                if(!$row["score1"] == '' || $row["matchDate"] < $_SESSION["todayDate"]) {
                                    echo "";
                                } else 
                                    echo $row["matchTime"] ?></td>

                    <td class='venue'><a href='ground.php?groundID=<?php echo $row["groundID"]?>' target="_blank"><?php echo $row["groundName"]?></a></td>
                </tr>
                <?php
                    }
                }
            }
                ?>
            </table>
        </div>
    </body>
</html>