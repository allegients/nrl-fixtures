/* Student ID: 18408339
 * Name: Timon Min Leung
 * Tutor's Name: Paul Davies
 */

var redScore1Checked = false;       // Whether or not another error already changed the border colour
var redScore2Checked = false;

/** 
 * The following code was modified from Week 5 Lecture Notes, Example 6 Examples from vUWS
 * https://vuws.westernsydney.edu.au/bbcswebdav/pid-2739464-dt-content-rid-22631257_1/xid-22631257_1
 ----------
 Client side validation for score entry
 */
function validateForm(form) {
    var valid = true;
    var inputValidator = [];

    inputValidator[0] = score1Validator();
    inputValidator[1] = score2Validator();

    for (var i = 0;  i < inputValidator.length; i++ ) {   
        if (!inputValidator[i]) {                    
            valid = false;
        }
    }

    if (!form.score1.value.length) { // Checking score 1 length
        valid = false;
        document.getElementById('score1_Error').style.display = "inline-block";
        if (!redScore1Checked) {
            form.score1.style.border = "1px solid #ff0000";
        }
    } 
    else {
        document.getElementById('score1_Error').style.display = "none";
        if (!redScore1Checked) {
            form.score1.style.border = "1px solid #ccc";
        }
    }

    if (!form.score2.value.length) { // Checking score 2 length
        valid = false;
        document.getElementById('score2_Error').style.display = "inline-block";
        if (!redScore2Checked) {
            form.score2.style.border = "1px solid #ff0000";
        }
    } 
    else {
        document.getElementById('score2_Error').style.display = "none";
        if (!redScore2Checked) {
            form.score2.style.border = "1px solid #ccc";
        }
    }
    
    if (!valid) {
        return false;
    }
}   // End of code


function score1Validator() {      // Checking if the score is a positive number
    var verify = /^[0-9]{0,10}$/;  
    if (score1.value.match(verify)) {
        document.getElementById('score1Input_Error').style.display = "none";
        score1.style.border = "1px solid #ccc";
        return true;
    }
    document.getElementById('score1Input_Error').style.display = "inline-block";
    score1.style.border = "1px solid #ff0000";
    redScore1Checked = true;
    return false;
}


function score2Validator() {
    var verify = /^[0-9]{0,10}$/;  
    if (score2.value.match(verify)) {
        document.getElementById('score2Input_Error').style.display = "none";
        score2.style.border = "1px solid #ccc";
        return true;
    }
    document.getElementById('score2Input_Error').style.display = "inline-block";
    score2.style.border = "1px solid #ff0000";
    redScore2Checked = true;
    return false;
}