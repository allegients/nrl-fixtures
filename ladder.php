<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<?php
require_once("nocache.php");
require_once('dbconn.php');
session_start();
?> 

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
        <title>NRL Fixtures</title>
    </head>
    <header id="nav" class="centre">
        <ul>
            <!-- The following code was inspired from https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar -->
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn">Fixtures</a>
                <div class="dropdown-content">
                    <a href="roundFixtures.php">Rounds Fixtures</a>
                    <a href="teamFixtures.php">Team Fixtures</a>
                </div>
            </li> <!-- End of code -->
            <li><a href="ladder.php">Ladder</a></li>
            <li><a href="scoreEntry.php">Enter Results</a></li>
            <?php 
            if(isset($_SESSION['who'])) { ?>
            <li><a href="logoff.php">Log Off</a></li>
            <?php
            }
            ?>
        </ul>
    </header>
    <body>
        <div class="centre">
            <h1>2017 NRL Ladder</h1>
            <table>
                <tr>
                    <th>Pos</th>
                    <th>Team</th>
                    <th>P</th>
                    <th>W</th>
                    <th>L</th>
                    <th>D</th>
                    <th>B</th>
                    <th>F</th>
                    <th>A</th>
                    <th>+/-</th>
                    <th>Pts</th>
                </tr>

                <?php   // Query database for ladder values
                $sql = "SELECT teamName, teamMascot, logo, played, won, lost, points, bye, team.for, against, drawn, scoreDiff
            FROM team
            ORDER BY points DESC, scoreDiff DESC";
                $results = mysqli_query($dbConn, $sql)
                    or die ('Problem with query' . mysqli_error());
                $pos = 1;   // Counter for position column
                while ($row = mysqli_fetch_array($results)) {
                ?>
                <tr>
                    <td><?php echo $pos?></td>
                    <td><img src="images/<?php echo $row["logo"]?>" style="width:30px;"><?php echo $row["teamName"] . " " . $row["teamMascot"];?></td>
                    <td><?php echo $row["played"]?></td>
                    <td><?php echo $row["won"]?></td>
                    <td><?php echo $row["lost"]?></td>
                    <td><?php echo $row["drawn"]?></td>
                    <td><?php echo $row["bye"]?></td>
                    <td><?php echo $row["for"]?></td>
                    <td><?php echo $row["against"]?></td>
                    <td><?php echo $row["scoreDiff"]?></td>
                    <td><?php echo $row["points"]?></td>
                    <?php 
                    $pos++;
                }
                    ?>
                </tr>
            </table>
        </div>
    </body>
</html>