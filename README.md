# README #

### NRL Fixtures ###
A website where users can view the score and leader board for the NRL games. Administrators of the account will login and enter scores accordingly to the results of the games. This will update the database which will be reflected on the leader board, round fixtures and team fixtures. Normal users will only have permission to view the scoreboards. 

### index.php ###
Homepage where users can see results by date.

### ground.php ###
This will find the stadium of the NRL teams and show it to the user on Google Maps.

### ladder.php ###
The leaderboard of all the NRL teams ranked accordingly.

### roundFixtures.php ###
Users can view results of specific rounds and teams.

### scoreEntry.php ###
Administrators can enter the results of matches here.

### teamFixtures.php###
Users can view all match results of a specific team.
