<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<?php
// ensure the page is not cached
require_once("nocache.php");
require_once('dbConn.php');     // Connecting to the database

// get access to the session variables
session_start();

$score1 = "";
$score1Error = "";
$scoreValid = "true";
$score2 = "";
$score2Error = "";

// check if the user is logged in
if (!$_SESSION["who"]){
    header("location: login.php");
}

/* Sessions to check whether the user has began entering scores or not
in case they leave the page and return
*/
if(!isset($_SESSION['scoreEntryStart'])) {
    $_SESSION['scoreEntryStart'] = "1";
    $_SESSION['scoreEntryCounter'] = "0";
    $i = 0;
    $_SESSION['homeTeam'] = "";
    $_SESSION['awayTeam'] = "";
    $_SESSION['matchIDUpdate'] = "";

}

// Session counter for counting which matchID the submission is up to
if(isset($_POST['Update_Scores'])) {

    /* The following code was inspired from Week 10 Lecture Notes
    https://vuws.westernsydney.edu.au/bbcswebdav/pid-2766621-dt-content-rid-22844837_1/xid-22844837_1
    -------------
    Server sided validation for score entry
    */
    $score1 = $_POST["score1"];
    if ($score1 == '') {
        $score1Error = '<span class="error"> Please input a value into Score 1.</span>';
        $scoreValid = false;
    }
    else {
        $pattern = "/^[0-9]{0,10}$/";
        if (!preg_match($pattern, $score1)) {
            $score1Error = '<span class="error"> Please only input POSITIVE NUMBERS in Score 1.</span>';
            $scoreValid = false;
        }
    }

    $score2 = $_POST["score2"];
    if ($score2 == '') {
        $score2Error = '<span class="error"> Please input a value into Score 2.</span>';
        $scoreValid = false;
    }
    else {
        $pattern = "/^[0-9]{0,10}$/";
        if (!preg_match($pattern, $score2)) {
            $score2Error = '<span class="error"> Please only input POSITIVE NUMBERS in Score 2.</span>';
            $scoreValid = false;
        }
    }   // End of code

    if ($scoreValid) {
        $homeTeamID = $_SESSION['homeTeam']; 
        $awayTeamID = $_SESSION['awayTeam'];
        $matchIDUpdate = $_SESSION['matchIDUpdate'];
        $winTeam = "";

        // Updating score for the match
        $sql = "UPDATE fixtures
    SET score1 = '$score1', score2 = '$score2'
    WHERE matchID = '$matchIDUpdate'";
        if(!mysqli_query($dbConn, $sql)){
            echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
        }

        if ($score1 > $score2) {      
            // Updating ladder for home team if they win
            $sql = "UPDATE team
        SET played = played + '1', won = won + '1', points = points + '2', team.for = team.for + '$score1', team.against = team.against + '$score2', scoreDiff = team.for - team.against
        WHERE teamID = '$homeTeamID'";
            if(!mysqli_query($dbConn, $sql)){
                echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
            }

            // Updating ladder for away team if they lose
            $sql = "UPDATE team
        SET played = played + '1', lost = lost + '1', team.for = team.for + '$score1', team.against = team.against + '$score2', scoreDiff = team.for - team.against
        WHERE teamID = '$awayTeamID'";
            if(!mysqli_query($dbConn, $sql)){
                echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
            }
        }
        else if ($score1 < $score2) {     
            // Updating ladder for away team if they win
            $sql = "UPDATE team
        SET played = played + '1', won = won + '1', points = points + '2', team.for = team.for + '$score1', team.against = team.against + '$score2', scoreDiff = team.for - team.against
        WHERE teamID = '$awayTeamID'";
            if(!mysqli_query($dbConn, $sql)){
                echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
            }

            // Updating ladder for home team if they lose
            $sql = "UPDATE team
        SET played = played + '1', lost = lost + '1', team.for = team.for + '$score1', team.against = team.against + '$score2', scoreDiff = team.for - team.against
        WHERE teamID = '$homeTeamID'";
            if(!mysqli_query($dbConn, $sql)){
                echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
            }
        }
        else if ($score1 == $score2) {        
            // Updating ladder for away team if it's a draw
            $sql = "UPDATE team
        SET played = played + '1', drawn = drawn + '1', points = points + '1', team.for = team.for + '$score1', team.against = team.against + '$score2', scoreDiff = team.for - team.against
        WHERE teamID = '$awayTeamID'";
            if(!mysqli_query($dbConn, $sql)){
                echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
            }

            // Updating ladder for home team it's a draw
            $sql = "UPDATE team
        SET played = played + '1', drawn = drawn + '1', points = points + '1', team.for = team.for + '$score1', team.against = team.against + '$score2', scoreDiff = team.for - team.against
        WHERE teamID = '$homeTeamID'";
            if(!mysqli_query($dbConn, $sql)){
                echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
            }
        }
        $_SESSION['scoreEntryCounter']++;
    }
}

// Setting the variable for the array in matchID for it to query the SQL database
if(isset($_SESSION['scoreEntryStart'])) {
    $i = $_SESSION['scoreEntryCounter'];
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="script.js" type="text/javascript" defer></script>
        <link rel="stylesheet" href="styles.css">
        <title>Score Entry</title>
    </head>
    <header id="nav" class="centre">
        <ul>
            <!-- The following code was inspired from https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar -->
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn">Fixtures</a>
                <div class="dropdown-content">
                    <a href="roundFixtures.php">Rounds Fixtures</a>
                    <a href="teamFixtures.php">Team Fixtures</a>
                </div>
            </li> <!-- End of code -->
            <li><a href="ladder.php">Ladder</a></li>
            <li><a href="scoreEntry.php">Enter Results</a></li>
            <?php 
            if(isset($_SESSION['who'])) { ?>
            <li><a href="logoff.php">Log Off</a></li>
            <?php
            }
            ?>
        </ul>
    </header>
    <body>
        <div class="centre">
            <form id="submit-scores" onsubmit="return validateForm(this);" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
                <?php 
                $round = $_SESSION['currentRound'];     // Passing variable for current round

                $sql = "SELECT roundID, matches
        FROM round
        WHERE roundID = '$round'";
                $results = mysqli_query($dbConn, $sql)
                    or die ('Problem with query' . mysqli_error());
                while ($row = mysqli_fetch_array($results)) {
                    $_SESSION['matches'] = $row['matches'];     // Finding how many matches in the round
                }

                if ($i < $_SESSION['matches']) {    // Counter for when all matches scores are entered
                    echo "<h1>You are entering scores for Round <a><i>" . $round . "</i></a></h1>";

                    echo $score1Error;
                    echo $score2Error;

                    $sql = "SELECT matchID, roundID
        FROM fixtures
        WHERE roundID = '$round'";
                    $results = mysqli_query($dbConn, $sql)
                        or die ('Problem with query' . mysqli_error());
                    while ($row = mysqli_fetch_array($results)) {  // Creating an array and
                        $matchID[] = $row['matchID'];              // placing all matchIDs in it
                    }

                    /* The following code was inspired from 
            http://stackoverflow.com/questions/42189707/sql-select-query-to-display-name-from-another-table-in-two-seperate-columns
            This will extract all the details for the matchID
            */
                    $sql = "SELECT f.matchID, t1.teamID AS homeTeamID, t1.abbreviatedName AS homeTeam, t1.logo AS homeLogo, t2.teamID AS awayTeamID, t2.abbreviatedName AS awayTeam, t2.logo AS awayLogo, f.roundID, matchDate, score1
                FROM fixtures f
                JOIN team t1 ON f.homeTeam = t1.teamID
                JOIN team t2 ON f.awayTeam = t2.teamID
                WHERE f.roundID = '$round' AND f.matchID = '$matchID[$i]'"; // End of code
                    $results = mysqli_query($dbConn, $sql)
                        or die ('Problem with query' . mysqli_error());
                    
                    /* This message will show if the scores for the game already exists and
                    the user is trying to submit one again for it thus causing the ladder to add more ladder values despite already calculated beforehand. In other words the amount of fixtures and ladder games played will be different
                    */
                    while ($row = mysqli_fetch_array($results)) {
                        if ($row['score1'] != '') {
                            echo "<p>NOTE: You are trying to submit a score to a match where the scores already exists and calculated. This will result in the ladder to have more games played versus actual games played displayed in the fixtures thus the ladder will show incorrect data.</p>";
                        }
                ?>
                <table>
                    <tr>
                        <th>Match</th>
                        <th>Home Team</th>
                        <th>Score 1</th>
                        <th>Score 2</th>
                        <th>Away Team</th>
                        <th>Date</th>
                    </tr>
                    <tr>
                        <td><?php echo $row['matchID'] ?></td>
                        <td><img src="images/<?php echo $row["homeLogo"]?>" style="width:60px;"><?php echo $row["homeTeam"]?></td>

                        <td><input type="text" id="score1" name="score1" size="5" maxlength="4"></td>
                        <span class="error_Messages" id="score1_Error">A number is required in Score 1</span>
                        <span class="error_Messages" id="score1Input_Error">Score 1 must be a positive <em>number.</em></span>

                        <td><input type="text" id="score2" name="score2" size="5" maxlength="4"></td>
                        <span class="error_Messages" id="score2_Error">A number is required in Score 2</span>
                        <span class="error_Messages" id="score2Input_Error">Score 2 must be a positive <em>number.</em></span>

                        <td><?php echo $row["awayTeam"]?><img src="images/<?php echo $row["awayLogo"]?>" style="width:60px;"></td>
                        <td><?php echo $row["matchDate"]?></td>
                    </tr>
                </table>
                <p><input type="submit" id="submit-scores" name="Update Scores" value="submit"></p>
                <?php 
                    $_SESSION['homeTeam'] = $row["homeTeamID"];
                        $_SESSION['awayTeam'] = $row["awayTeamID"];
                        $_SESSION['matchIDUpdate'] = $row["matchID"];
                    }
                }
                else {  // Once all matches scores are entered this will execute
                    echo "<h1>Successfully entered scores for all matches in round " . $round . "</h2>";

                    // Adding byes from the round
                    $sql = "SELECT teamID
                FROM byes
                WHERE roundID = '$round'";
                    $results = mysqli_query($dbConn, $sql)
                        or die ('Problem with query' . mysqli_error());
                    while ($row = mysqli_fetch_array($results)) {
                        $byeTeam = $row['teamID'];

                        // Updating byes for the team
                        $sql = "UPDATE team
                    SET bye = bye + '1', points = points + '2'
                    WHERE teamID = '$byeTeam'";
                        if(!mysqli_query($dbConn, $sql)){
                            echo "ERROR: Could not update $sql. " . mysqli_error($dbConn);
                        }
                    }
                    unset($_SESSION['scoreEntryStart']);
                }
                ?>
            </form>
        </div>
    </body>
</html>
