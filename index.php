<!-- Student ID: 18408339
Name: Timon Min Leung
Tutor's Name: Paul Davies -->

<?php
require_once("nocache.php");
require_once('dbConn.php');     // Connecting to the database
session_start();
$today = date("Y-m-d");   //get the server date in the correct format
$_SESSION["todayDate"] = $today;

/* The following code was inspired from
http://stackoverflow.com/questions/9682229/run-the-code-only-once-in-php
*/
if (!isset($_SESSION["userInputted"])) {     // If the user does not select for current or
    $_SESSION["userInputted"] = "1";        // specific round in index.php the website will
    $choice = "server";                     // default to using current round
    $_POST["submit"] = "1";
}   // End of code

if (isset($_POST["submit"])){
    if ($_POST["submit"] != 1) {
        $choice = $_POST["choice"];
    }
    //set the round number based upon the users choice
    if ($choice == "server"){
        $validDate = false;
        //query the db here for the round number based on the server date
        $sql = "SELECT roundID, startDate, endDate
        FROM round
        WHERE '$today' BETWEEN round.startDate AND round.endDate";
        $results = mysqli_query($dbConn, $sql)
            or die ('Problem with query' . mysqli_error());
        while ($row = mysqli_fetch_array($results)) { 
            $todaysRound = $row["roundID"];
            $_SESSION["currentRound"] = $todaysRound;
            $validDate = true;
        }

        /* The following code is inspired from 
        http://stackoverflow.com/questions/22500610/sql-get-the-closest-row-for-a-certain-date-in-other-table
        */
        if (!$validDate) {      // If the current date is not within a round it will find the next upcoming round
            $sql = "SELECT roundID, startDate 
            FROM round
            WHERE round.startDate > '$today'
            ORDER BY round.startDate ASC LIMIT 1";      // End of code
            $results = mysqli_query($dbConn, $sql)
                or die ('Problem with query' . mysqli_error());
            while ($row = mysqli_fetch_array($results)) { 
                $todaysRound = $row["roundID"];
                $_SESSION["currentRound"] = $todaysRound;
            }
        }
    }
    else {
        $round = $_POST["roundNum"];
        $_SESSION["currentRound"] = $round;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
        <title>NRL Assignment - Choose Round</title>
        <script>
            function changeSelectionList(){
                document.getElementById('submit-form').disabled = false;
                if (document.getElementById("roundForm").choice.value == "server")
                    document.getElementById("roundNum").disabled = true;
                else
                    document.getElementById("roundNum").disabled = false;
            }
        </script>
    </head>
    <header id="nav" class="centre">
        <ul>
            <!-- The following code was inspired from https://www.w3schools.com/css/tryit.asp?filename=trycss_dropdown_navbar -->
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn">Fixtures</a>
                <div class="dropdown-content">
                    <a href="roundFixtures.php">Rounds Fixtures</a>
                    <a href="teamFixtures.php">Team Fixtures</a>
                </div>
            </li> <!-- End of code -->
            <li><a href="ladder.php">Ladder</a></li>
            <li><a href="scoreEntry.php">Enter Results</a></li>
            <?php 
            if(isset($_SESSION['who'])) { ?>
            <li><a href="logoff.php">Log Off</a></li>
            <?php
            }
            ?>
        </ul>
    </header>
    <body>
        <div class="centre">
            <h1>NRL Ladder Assignment</h1>

            <?php
            if(isset($_SESSION['errorMessage'])) {
                echo "<p style='color:red;'>" . $_SESSION['errorMessage'] . "</p>";
                $_SESSION['errorMessage'] = "";
            }
            ?>
            <form id="roundForm" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
                <p>Do you want to use the Server Date or User Input for the current round?</p>

                <p>
                    <label for="Server">Server Date</label>
                    <input type="radio" id="Server" name="choice" value="server" onclick="changeSelectionList();">
                </p>  

                <p>
                    <label for="User">User Input</label>
                    <input type="radio" id="User" name="choice" value="user" onclick="changeSelectionList();">
                </p>

                <p>
                    <label for="roundNum">Round Number:</label>     
                    <select id="roundNum" name="roundNum" size="1" disabled>
                        <script>
                            for (i = 1; i <= 26; i++)
                                document.write('<option value="' + i + '">' + i + '</option>');
                        </script>
                    </select>
                </p>
                <div class="centreSubmit"><input type="submit" id="submit-form" name="submit" value="Submit" disabled></div>
            </form>
        </div>
    </body>
</html>