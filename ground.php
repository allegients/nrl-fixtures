<?php
require_once("nocache.php");    // Making sure website has no cache
require_once('dbConn.php');     // Connecting to the database
?> 

<!-- The following code is modified from
https://developers.google.com/maps/documentation/javascript/adding-a-google-map -->
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
        <style>
            #map {
                height: 400px;
                width: 700px;
            }
        </style>
        <title>Venue Location</title>
    </head>
    <body>
        <div class="centre">

            <?php   // Querying database to receive selected groundID's information
            $strGroundID = $_GET["groundID"];   // Obtaining groundID from hyperlink
            $sql = "SELECT groundID, groundName, address, latitude, longitude
      FROM ground
      WHERE ground.groundID = '$strGroundID'";
            $results = mysqli_query($dbConn, $sql)
                or die ('Problem with query' . mysqli_error());
            while ($row = mysqli_fetch_array($results)) {
                $strGroundName = $row["groundName"];
                $strAddress = $row["address"];
                $strLatitude = $row["latitude"];
                $strLongitude = $row["longitude"]; 
            } 
            ?>

            <h1>Venue Location</h1>
            <p><?php echo $strGroundName ?></p>
            <p><?php echo $strAddress ?></p>
            <div id="map"></div>
            <script>
                function initMap() {
                    var venue = {lat: <?php echo $strLatitude ?>, lng: <?php echo $strLongitude ?>};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 13,
                        center: venue
                    });
                    var marker = new google.maps.Marker({
                        position: venue,
                        map: map
                    });
                }
            </script>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfl-LHRKxIUo0mQX1I8eDu-Vs9k888eNc&callback=initMap">
            </script>
        </div>
    </body>
</html> <!-- End of code -->